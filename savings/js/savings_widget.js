/*
 Rootsteps savings widget script
 */

new Vue({
    el: '.widget--savings',
    data: {
        numVehicles: 1,
        annualKms: 0,
        peoplePerVehicle: 1,
        fuel: 'Diesel',
        hourlyRate: document.querySelector('#hourlyRate').getAttribute('value'),
        petrolPrice: document.querySelector('#petrolPrice').getAttribute('value'),
        dieselPrice: document.querySelector('#dieselPrice').getAttribute('value')
    },
    computed: {
        fuelPrice: function () {
            if (this.fuel == 'Diesel') {
                return this.dieselPrice;
            } else {
                return this.petrolPrice;
            }
        },
        monthlySavingsOnWages: function () {
            // `this` points to the vm instance
            return (this.numVehicles * this.peoplePerVehicle * this.hourlyRate * 0.5 * 225) / 12;
        },
        savingsOnKms: function () {
            // `this` points to the vm instance
            return ((this.annualKms * 0.05 / 10) * this.fuelPrice) * this.numVehicles;
        },
        savingsOnStationary: function () {
            // `this` points to the vm instance
            return (((this.numVehicles * 0.25 * 225) / 12) / 1.5) * this.fuelPrice;
        },
        totalMonthlySavings: function () {
            // `this` points to the vm instance
            return this.monthlySavingsOnWages + this.savingsOnKms + this.savingsOnStationary;
        },
        savingsPerVehicle: function () {
            // `this` points to the vm instance
            return this.totalMonthlySavings / this.numVehicles;
        }
    }
});
