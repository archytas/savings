<?php

/*
Plugin Name: Savings
Plugin URI: https://php-freelance.nl
Description: Widget om de besparing te berekenen
Version: 1.0.0
Author: Archytas
Author URI: https://www.archytas.nl/
License: all rights reserved
Text Domain: archytas
*/

define('SAVINGS_DEFAULT_OPTIONS', [
    'title' => 'Savings widget',
    'petrol_price' => 1.65,
    'diesel_price' => 1.35,
    'hourly_rate' => 18,
]);

// class name is a little too generic
class Savings extends WP_Widget
{

    function __construct()
    {
        $id_base = 'savings';
        $name = __('Savings calculator');
        $widget_options = [];
        $control_options = [];
        parent::__construct($id_base, $name, $widget_options, $control_options);
    }

    /**
     * Display the widget
     * @param array $args
     * @param array $instance
     */
    function widget($args, $instance)
    {
        $savings_options = get_option('savings_options', SAVINGS_DEFAULT_OPTIONS);
        ?>
        <div class="widget widget--savings">
            <h2 class="widget-title"><?php echo $savings_options['title'] ?></h2>

            <div>
                <form>
                    <input id="petrolPrice" type="hidden" value="<?php echo $savings_options['petrol_price'] ?>">
                    <input id="dieselPrice" type="hidden" value="<?php echo $savings_options['diesel_price'] ?>">
                    <input id="hourlyRate" type="hidden" value="<?php echo $savings_options['hourly_rate'] ?>">

                    <div class="form-group">
                        <label for="numVehicles">Aantal voertuigen</label>
                        <input type="number" class="form-control" id="numVehicles" v-model.number="numVehicles"
                               placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="annualKms">Aantal kilometers per jaar</label>
                        <input type="number" class="form-control" id="annualKms" v-model.number="annualKms" placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="peoplePerVehicle">Aantal medewerkers op een voertuig</label>
                        <input type="number" class="form-control" id="peoplePerVehicle" v-model.number="peoplePerVehicle"
                               placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="fuel">Brandstof</label>
                        <select class="form-control" id="fuel" v-model="fuel">
                            <option>Diesel</option>
                            <option>Benzine</option>
                        </select>
                    </div>

                </form>

                <div class="report">
                    <div class="arrow-down"></div>
                    <div>
                        <span class="label">Besparing op loonkosten p.m.: </span> &euro; {{ monthlySavingsOnWages.toFixed(2)
                        }}
                    </div>
                    <div>
                        <span class="label">Besparing op kilometers: </span> &euro; {{ savingsOnKms.toFixed(2) }}
                    </div>
                    <div>
                        <span class="label">Besparing op stationair: </span> &euro; {{ savingsOnStationary.toFixed(2) }}
                    </div>
                    <div>
                        <span class="label">Besparing p.m. in totaal: </span> &euro; {{ totalMonthlySavings.toFixed(2) }}
                    </div>
                    <div>
                        <span class="label">Besparing per voertuig: </span> &euro; {{ savingsPerVehicle.toFixed(2) }}
                    </div>
                </div>
            </div>

        </div>
        <?php
    }

}

/**
 * Register widget upon initialization
 */
add_action('widgets_init', function () {
    register_widget('Savings');
});

/**
 * Add assets (js, css)
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('savings_widget', plugins_url('savings/css/widget.css'));
    wp_enqueue_script('vue', plugins_url('savings/js/vue.min.js'));
    wp_enqueue_script('savings_widget', plugins_url('savings/js/savings_widget.js'), [], false, true);
});

/**
 * Admin page
 */
add_action('admin_menu', function () {
    add_options_page('Savings widget settings', 'Savings', 'manage_options', 'plugin', 'savings_options_page');
});

function savings_options_page()
{
    ?>
    <div>
        <h2>Savings plugin</h2>
        <p>Instellingen voor de widget om de besparingen voor het wagenpark te berekenen.</p>

        <form action="options.php" method="post">
            <?php settings_fields('savings_options'); ?>
            <?php do_settings_sections('savings'); ?>

            <input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>"/>
        </form>
    </div>

    <?php
}

add_action('admin_init', function () {
    register_setting('savings_options', 'savings_options', 'savings_options_validate');
    add_settings_section('savings_main', 'Algemene instellingen', 'savings_section_text', 'savings');

    add_settings_field('savings_title', 'Titel', 'savings_setting_title', 'savings', 'savings_main');
    add_settings_field('savings_hourly_rate', 'Uurtarief', 'savings_setting_hourly_rate', 'savings', 'savings_main');
    add_settings_field('savings_diesel_price', 'Dieselprijs', 'savings_setting_diesel_price', 'savings', 'savings_main');
    add_settings_field('savings_petrol_price', 'Benzineprijs', 'savings_setting_petrol_price', 'savings', 'savings_main');
});

function savings_section_text()
{
    echo 'U kunt de titel, het uurtarief en de actuele brandstofprijzen hieronder aanpassen.';
}

function savings_setting_title()
{
    $options = get_option('savings_options', SAVINGS_DEFAULT_OPTIONS);
    echo "<input id='savings_setting_title' name='savings_options[title]' size='40' type='text' value='{$options['title']}' />";
}

function savings_setting_hourly_rate()
{
    $options = get_option('savings_options', SAVINGS_DEFAULT_OPTIONS);
    echo "<input id='savings_setting_hourly_rate' name='savings_options[hourly_rate]' size='10' type='text' value='{$options['hourly_rate']}' />";
}

function savings_setting_diesel_price()
{
    $options = get_option('savings_options', SAVINGS_DEFAULT_OPTIONS);
    echo "<input id='savings_setting_diesel_price' name='savings_options[diesel_price]' size='10' type='text' value='{$options['diesel_price']}' />";
}

function savings_setting_petrol_price()
{
    $options = get_option('savings_options', SAVINGS_DEFAULT_OPTIONS);
    echo "<input id='savings_setting_petrol_price' name='savings_options[petrol_price]' size='10' type='text' value='{$options['petrol_price']}' />";
}

function savings_options_validate($input)
{
    // @todo: Validate input (left out as a time saver)
    // as it is only edited by the admin, it will not be a security thread, however
    // he can potentially break the rendering of the site is he enters malformed input.

    return $input;
}